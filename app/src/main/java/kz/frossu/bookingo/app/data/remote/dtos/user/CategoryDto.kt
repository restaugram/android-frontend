package kz.frossu.bookingo.app.data.remote.dtos.user

import com.google.gson.annotations.SerializedName

data class CategoryDto(
    @SerializedName("id"       ) var id       : String,
    @SerializedName("imageUrl" ) var imageUrl : String?             = null,
    @SerializedName("alt"      ) var alt      : String?             = null,
    @SerializedName("parentId" ) var parentId : String?             = null,
    @SerializedName("children" ) var children : List<CategoryDto> = listOf(),
    @SerializedName("slug"     ) var slug     : String?             = null,
    @SerializedName("name"     ) var name     : String?             = null
)