package kz.frossu.bookingo.app.presentation.ui.fragments.auth.user.sign.`in`

/**
 * Authentication result : success (user details) or error message.
 */
data class UserLoginResult(
    val success: LoggedInUserView? = null,
    val error: Int? = null
)