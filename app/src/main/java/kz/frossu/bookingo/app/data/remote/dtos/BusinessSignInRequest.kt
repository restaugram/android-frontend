package kz.frossu.bookingo.app.data.remote.dtos

import com.google.gson.annotations.SerializedName

data class BusinessSignInRequest(
    @field:SerializedName("businessEmail") var email: String,
    @field:SerializedName("password") var password: String,
)
