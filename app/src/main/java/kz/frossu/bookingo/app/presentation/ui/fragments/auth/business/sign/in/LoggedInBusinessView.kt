package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.`in`

/**
 * User details post authentication that is exposed to the UI
 */
data class LoggedInBusinessView(
    val displayName: String
    //... other data fields that may be accessible to the UI
)