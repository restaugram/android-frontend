package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.`in`

/**
 * Authentication result : success (user details) or error message.
 */
data class BusinessLoginResult(
    val success: LoggedInBusinessView? = null,
    val error: Int? = null
)