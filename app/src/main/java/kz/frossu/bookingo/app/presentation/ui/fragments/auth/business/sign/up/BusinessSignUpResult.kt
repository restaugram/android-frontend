package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.up

class BusinessSignUpResult(
    val success: Int? = null,
    val error: String? = null
)
