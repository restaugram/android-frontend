package kz.frossu.bookingo.app.presentation.ui.fragments.auth.user.sign.`in`

/**
 * Data validation state of the userLogin form.
 */
data class UserLoginFormState(
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val isDataValid: Boolean = false
)