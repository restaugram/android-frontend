package kz.frossu.bookingo.app.presentation.extensions

import android.os.Handler
import android.os.SystemClock
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import okhttp3.ResponseBody


@Suppress("BlockingMethodInNonBlockingContext")
suspend fun ResponseBody.stringSuspending() =
    withContext(Dispatchers.IO) { string() }


fun Handler.postDelayedWithToken(token: Any, delayInMillis: Long, runnable: Runnable) {
    postAtTime(runnable, token, SystemClock.uptimeMillis() + delayInMillis)
}
