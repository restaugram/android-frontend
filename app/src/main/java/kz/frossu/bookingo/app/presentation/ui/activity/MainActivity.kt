package kz.frossu.bookingo.app.presentation.ui.activity

import android.app.UiModeManager.MODE_NIGHT_YES
import android.os.Bundle
import android.widget.Button
import android.widget.TextView
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.appcompat.app.AppCompatDelegate.MODE_NIGHT_NO
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.view.GravityCompat
import androidx.drawerlayout.widget.DrawerLayout
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.NavigationUI.setupActionBarWithNavController
import androidx.navigation.ui.NavigationUI.setupWithNavController
import dagger.hilt.android.AndroidEntryPoint
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.local.preferences.PreferencesHelper
import kz.frossu.bookingo.app.databinding.ActivityMainBinding
import kz.frossu.bookingo.app.presentation.extensions.showToastLong
import kz.frossu.bookingo.app.presentation.ui.fragments.map.MapFragmentDirections
import javax.inject.Inject

@AndroidEntryPoint
class MainActivity /*@Inject constructor(
    private val preferencesHelper: PreferencesHelper
)*/ : AppCompatActivity() {

    @Inject
    lateinit var preferencesHelper: PreferencesHelper

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MapsViewModel by viewModels()

    private lateinit var navController: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController
        setupActionBarWithNavController(this, navController, binding.drawerLayout)
        setupWithNavController(binding.navView, navController)

        setupDrawer()
        setupNavigation()
    }

    private fun setupNavigation() {

        supportActionBar?.setDisplayShowTitleEnabled(false)

        navController.addOnDestinationChangedListener { controller, destination, _ ->
            if (destination.id == controller.graph.startDestinationId) {
                binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED)
            } else {
                binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED)
            }

            if (destination.id == R.id.businessDetailsFragment) {
                supportActionBar?.show()
            } else {
                supportActionBar?.hide()
            }
        }
    }

    override fun onSupportNavigateUp(): Boolean {
        return navController.navigateUp() || super.onSupportNavigateUp()
    }

    private fun setupDrawer() {
        setupDrawerHeaderView()
        setupDrawerItemSelectedListener()
    }

    private fun setupDrawerHeaderView() {
        val drawerView = binding.navView
        drawerView.inflateHeaderView(R.layout.loading_header_navigation_drawer)
        viewModel.currentUser.observe(this) { currentUser ->
            drawerView.removeHeaderView(drawerView.getHeaderView(0))
            if (currentUser == null) {
                val headerView =
                    drawerView.inflateHeaderView(R.layout.signed_out_header_navigation_drawer)
                val signInButton = headerView.findViewById<Button>(R.id.sign_in_button)
                signInButton.setOnClickListener {
                    val action = MapFragmentDirections.actionMapFragmentToAuthFragment()
                    navController.navigate(action)
                }
            } else {
                val headerView =
                    drawerView.inflateHeaderView(R.layout.signed_in_header_navigation_drawer)
                val name = headerView.findViewById<TextView>(R.id.name)
                name.text = currentUser.displayName
                val signOutButton = headerView.findViewById<Button>(R.id.sign_out_button)
                signOutButton.setOnClickListener {
                    viewModel.signOut()
                }

                headerView.setOnClickListener {
                    showToastLong(if (currentUser.isClient) "Is client" else "Is business")
                }
            }
        }

        updateNightModeSwitcher()
    }

    private fun updateNightModeSwitcher() {
        val nightModeItem = binding.navView.menu.findItem(R.id.night_mode)
        if (isForcingNightMode()) {
            nightModeItem.setTitle(R.string.light_mode)
            nightModeItem.icon =
                AppCompatResources.getDrawable(applicationContext, R.drawable.ic_light)
        } else {
            nightModeItem.setTitle(R.string.night_mode)
            nightModeItem.icon =
                AppCompatResources.getDrawable(applicationContext, R.drawable.ic_night)
        }
    }

    private fun isForcingNightMode(): Boolean {
        return AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES
    }

    // TODO(android defense): 8.1 Application should be customizable by user
    private fun setupDrawerItemSelectedListener() {
        binding.navView.setNavigationItemSelectedListener { menuItem ->

            if (menuItem.itemId == R.id.night_mode) {

                val modeToSet = when (isForcingNightMode()) {
                    true -> MODE_NIGHT_NO
                    false -> MODE_NIGHT_YES
                }

                AppCompatDelegate.setDefaultNightMode(modeToSet)
                preferencesHelper.setNightMode(modeToSet)
                updateNightModeSwitcher()
            } else {
                menuItem.isChecked = true
                binding.drawerLayout.closeDrawer(GravityCompat.START)
            }
            true
        }
    }

    fun openDrawer() {
        binding.drawerLayout.openDrawer(GravityCompat.START)
    }

    fun isAuthorized(): Boolean {
        return viewModel.isUserAuthorized()
    }

    fun updateUser() {
        viewModel.fetchCurrentUserIfAuthorized()
    }

    override fun onBackPressed() {
        if (binding.drawerLayout.isDrawerOpen(GravityCompat.START)) {
            binding.drawerLayout.closeDrawer(GravityCompat.START)
        } else {
            super.onBackPressed()
        }
    }
}