package kz.frossu.bookingo.app.presentation.ui.fragments.auth.user.sign.`in`

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.data.repositories.LoginRepository
import kz.frossu.bookingo.app.presentation.models.LoggedInUser
import javax.inject.Inject

@HiltViewModel
class UserLoginViewModel @Inject constructor(
    private val loginRepository: LoginRepository
) : ViewModel() {

    private val _loginForm = MutableLiveData<UserLoginFormState>()
    val userLoginFormState: LiveData<UserLoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<UserLoginResult>()
    val userLoginResult: LiveData<UserLoginResult> = _loginResult

    fun login(username: String, password: String) {
        // can be launched in a separate asynchronous job
        viewModelScope.launch {

            when (val result = loginRepository.userLogin(username, password)) {
                is Result.Success<LoggedInUser> ->
                    _loginResult.value =
                        UserLoginResult(success = LoggedInUserView(displayName = result.data.displayName))
                else ->
                    _loginResult.value =
                        UserLoginResult(error = R.string.login_failed)
            }
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = UserLoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = UserLoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = UserLoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(username).matches()
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}