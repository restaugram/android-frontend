package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.up

class BusinessSignUpFormState(
    val emailError: Int? = null,
    val passwordError: Int? = null,
    val businessNameError: Int? = null,
    val businessDescription: Int? = null,
    val isDataValid: Boolean = false
)