package kz.frossu.bookingo.app.presentation.ui.fragments.map

import android.content.Context
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import androidx.activity.OnBackPressedCallback
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.material.bottomsheet.BottomSheetBehavior
import dagger.hilt.android.AndroidEntryPoint
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.Constants
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessInfo
import kz.frossu.bookingo.app.databinding.FragmentMapBinding
import kz.frossu.bookingo.app.databinding.SearchBottomSheetBinding
import kz.frossu.bookingo.app.presentation.ui.activity.MainActivity
import kz.frossu.bookingo.app.presentation.ui.activity.MapsViewModel
import kz.frossu.bookingo.app.presentation.ui.adapters.BusinessSearchResultsAdapter
import timber.log.Timber

@AndroidEntryPoint
class MapFragment : Fragment(), OnMapReadyCallback {
    private lateinit var mMap: GoogleMap
    private lateinit var searchBottomSheetBinding: SearchBottomSheetBinding
    private lateinit var searchBottomSheetBehavior: BottomSheetBehavior<View>
    private lateinit var searchRecyclerView: RecyclerView
    private lateinit var searchAdapter: BusinessSearchResultsAdapter

    private val viewModel: MapsViewModel by viewModels()
    private var _binding: FragmentMapBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!
    private val onBackPressedCallback =
        object : OnBackPressedCallback(true) {
            override fun handleOnBackPressed() {
                if (isShowingSearch) {
                    cancelSearch()
                } else {
                    this.isEnabled = false
                    requireActivity().onBackPressed()
                }
            }
        }

    private var isShowingSearch: Boolean = false

    private fun setupMap() {
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = childFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
    }

    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap
        // Add a marker in Sydney and move the camera
        val sydney = LatLng(-34.0, 151.0)
        mMap.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney))
    }

    private fun setupSearch() {
        searchRecyclerView = searchBottomSheetBinding.searchRecyclerView
        searchRecyclerView.layoutManager = LinearLayoutManager(context)
        searchAdapter = BusinessSearchResultsAdapter() // TODO(android defense): 7.4 Use RecyclerView to show list of information
        searchAdapter.clickHandler = { businessInfo: BusinessInfo ->
            searchBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            val action =
                MapFragmentDirections.actionMapFragmentToBusinessDetailsFragment(businessInfo)
            findNavController().navigate(action)
        }
        searchRecyclerView.adapter = searchAdapter
        searchBottomSheetBinding.searchBar.doOnTextChanged { text, _, _, _ ->
            viewModel.searchTextChanged(text)
        }
        viewModel.searchData.observe(viewLifecycleOwner) { businessInfoList ->
            showItems(businessInfoList)
        }
    }

    private fun showItems(businessInfoList: List<BusinessInfo>) {
        val switcher = searchBottomSheetBinding.switcher
        if (businessInfoList.isNotEmpty()) {
            searchAdapter.submitList(businessInfoList)

            if (R.id.search_recycler_view == switcher.nextView.id) {
                switcher.showNext()
            }
        } else if (R.id.tv_empty == switcher.nextView.id) {
            switcher.showNext()
        }
    }

    private fun setupBottomSheetBehavior() {
        searchBottomSheetBinding = binding.searchBottomSheetContainer
        searchBottomSheetBehavior =
            BottomSheetBehavior.from(searchBottomSheetBinding.searchBottomSheet)
        searchBottomSheetBehavior.isFitToContents = false
        val collapsedHeight = 228
        val density: Float = resources.displayMetrics.density
        searchBottomSheetBehavior.peekHeight = (collapsedHeight * density).toInt()
        searchBottomSheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
        searchBottomSheetBehavior.addBottomSheetCallback(bottomSheetCallback())

        searchBottomSheetBinding.showNavigationDrawerButton.setOnClickListener { openDrawer() }
        searchBottomSheetBinding.searchBarDummy.setOnClickListener { showSearch() }
        searchBottomSheetBinding.cancelButton.setOnClickListener { cancelSearch() }
    }

    private fun openDrawer() {
        (requireActivity() as MainActivity).openDrawer()
    }

    private fun bottomSheetCallback(): BottomSheetBehavior.BottomSheetCallback {
        return object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                if (isShowingSearch && (newState == BottomSheetBehavior.STATE_HALF_EXPANDED || newState == BottomSheetBehavior.STATE_COLLAPSED)) {
                    makeSearchInvisible()
                    unfocusSearchBar()
                    isShowingSearch = false
                } else if (isShowingSearch && newState == BottomSheetBehavior.STATE_EXPANDED) {
                    focusSearchBar()
                } else if (newState == BottomSheetBehavior.STATE_DRAGGING) {
                    unfocusSearchBar()
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
                with(searchBottomSheetBinding) {
                    if (slideOffset > 0 && isShowingSearch) {
                        layoutCollapsed.alpha =
                            if (slideOffset > 0.4) (1 - 0.5 * slideOffset).toFloat() else 1.0f
                        layoutExpanded.alpha = slideOffset * slideOffset

                        if (slideOffset > 0.7) {
                            makeSearchVisible()
                        }

                        if (slideOffset <= 0.7 && layoutExpanded.visibility == View.VISIBLE) {
                            makeSearchInvisible()
                        }
                    }
                }
            }
        }
    }

    private fun makeSearchVisible() {
        searchBottomSheetBinding.layoutCollapsed.visibility = View.GONE
        searchBottomSheetBinding.layoutExpanded.visibility = View.VISIBLE
    }

    private fun makeSearchInvisible() {
        searchBottomSheetBinding.layoutCollapsed.visibility = View.VISIBLE
        searchBottomSheetBinding.layoutExpanded.visibility = View.INVISIBLE
    }

    private fun showSearch() {
        makeSearchVisible()
        isShowingSearch = true
        searchBottomSheetBinding.layoutExpanded.alpha = 1.0f
        searchBottomSheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
        focusSearchBar()
    }

    private fun cancelSearch() {
        searchBottomSheetBehavior.state = BottomSheetBehavior.STATE_HALF_EXPANDED
    }

    private fun focusSearchBar() {
        if (searchBottomSheetBinding.searchBar.requestFocus()) {
            Handler(Looper.getMainLooper()).postDelayed({
                (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
                    .showSoftInput(
                        searchBottomSheetBinding.searchBar,
                        InputMethodManager.SHOW_IMPLICIT
                    )
            }, 200)
        }
    }

    private fun unfocusSearchBar() {
        searchBottomSheetBinding.searchBar.clearFocus()
        (requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager)
            .hideSoftInputFromWindow(searchBottomSheetBinding.searchBar.windowToken, 0)
    }

    private fun setupOnBackPressedCallback() {
        requireActivity().onBackPressedDispatcher.addCallback(
            viewLifecycleOwner,
            onBackPressedCallback
        )
    }

    // TODO(android defense): 1. Show all lifecycle components used

    // View layout only exists between onCreateView and onDestroyView
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentMapBinding.inflate(inflater)
        return binding.root
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    // Main work here
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupMap()
        setupBottomSheetBehavior()
        setupSearch()
        setupOnBackPressedCallback()
    }

    // in onResume and onPause we detach and reattach the onBackPressed callback from fragment
    override fun onResume() {
        super.onResume()
        onBackPressedCallback.isEnabled = true
    }

    override fun onPause() {
        super.onPause()
        onBackPressedCallback.isEnabled = false
    }

    // onViewStateRestored and onSaveInstanceState allow us to store
    // the state of bottom sheet across configuration changes (e.g. split screen, screen rotation)
    override fun onViewStateRestored(savedInstanceState: Bundle?) {
        super.onViewStateRestored(savedInstanceState)
        if (savedInstanceState != null && savedInstanceState.getBoolean(Constants.showingSearchLayoutKey)) {
            showSearch()
        }
    }

    override fun onSaveInstanceState(outState: Bundle) {
        Timber.d("onSaveInstanceState, search: $isShowingSearch")
        outState.putBoolean(Constants.showingSearchLayoutKey, isShowingSearch)
        super.onSaveInstanceState(outState)
    }
}