package kz.frossu.bookingo.app.presentation.ui.fragments.auth.user.sign.up

data class UserSignUpFormState(
    val emailError: Int? = null,
    val passwordError: Int? = null,
    val phoneError: Int? = null,
    val fullNameError: Int? = null,
    val isDataValid: Boolean = false
)