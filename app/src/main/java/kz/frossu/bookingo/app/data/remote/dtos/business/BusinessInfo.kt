package kz.frossu.bookingo.app.data.remote.dtos.business

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.parcelize.Parcelize

// TODO(android defense): 7.3 Use RecyclerView to show list of information
@Parcelize
data class BusinessInfo (
    @SerializedName("id"           ) var id           : String,
    @SerializedName("businessName" ) var businessName : String,
    @SerializedName("description"  ) var description  : String? = null,
    @SerializedName("categoryId"   ) var categoryId   : String? = null,
    @SerializedName("isSubscribed" ) var isSubscribed : Boolean = false,
) : Parcelable

