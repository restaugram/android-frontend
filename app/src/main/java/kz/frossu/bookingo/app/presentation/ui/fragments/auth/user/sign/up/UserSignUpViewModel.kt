package kz.frossu.bookingo.app.presentation.ui.fragments.auth.user.sign.up

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.data.remote.dtos.user.UserSignUpRequest
import kz.frossu.bookingo.app.data.repositories.LoginRepository
import java.time.ZoneId
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter
import javax.inject.Inject

@HiltViewModel
class UserSignUpViewModel @Inject constructor(
    private val loginRepository: LoginRepository
) : ViewModel() {

    private val _signUpForm = MutableLiveData<UserSignUpFormState>()
    val userSignUpFormState: LiveData<UserSignUpFormState> = _signUpForm

    private val _signUpResult = MutableLiveData<UserSignUpResult>()
    val userSignUpResult: LiveData<UserSignUpResult> = _signUpResult

    fun signUp(
        email: String,
        password: String,
        phoneNumber: String,
        fullName: String,
        birthDate: String? = null
    ) {
        viewModelScope.launch {
            val userSignUpRequest = UserSignUpRequest(
                email,
                fullName,
                phoneNumber,
                password,
                ZonedDateTime.now(ZoneId.of("UTC")).format(DateTimeFormatter.ISO_LOCAL_DATE)
            )

            when (val result = loginRepository.userSignUp(userSignUpRequest)) {
                is Result.Success -> _signUpResult.value =
                    UserSignUpResult(success = R.string.sign_up_successful)
                is Result.Error -> _signUpResult.value =
                    UserSignUpResult(error = result.exception.message)
            }
        }
    }

    fun signUpDataChanged(email: String, password: String, phoneNumber: String, fullName: String) {

        if (!isNameValid(fullName)) {
            _signUpForm.value = UserSignUpFormState(fullNameError = R.string.invalid_name)
        } else if (!isEmailValid(email)) {
            _signUpForm.value = UserSignUpFormState(emailError = R.string.invalid_email)
        } else if (!isPhoneNumberValid(phoneNumber)) {
            _signUpForm.value = UserSignUpFormState(phoneError = R.string.invalid_phone)
        } else if (!isPasswordValid(password)) {
            _signUpForm.value = UserSignUpFormState(passwordError = R.string.invalid_password)
        } else {
            _signUpForm.value = UserSignUpFormState(isDataValid = true)
        }
    }

    private fun isEmailValid(email: String): Boolean {

        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {

        return password.length > 5
    }

    private fun isPhoneNumberValid(phoneNumber: String): Boolean {

        return Patterns.PHONE.matcher(phoneNumber).matches()
    }

    private fun isNameValid(fullName: String): Boolean {

        return fullName.isNotBlank()
    }
}
