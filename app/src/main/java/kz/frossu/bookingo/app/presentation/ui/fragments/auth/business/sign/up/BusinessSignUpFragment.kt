package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.up

import android.app.TimePickerDialog
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.remote.dtos.user.CategoryDto
import kz.frossu.bookingo.app.databinding.FragmentBusinessSignUpBinding
import kz.frossu.bookingo.app.presentation.extensions.gone
import kz.frossu.bookingo.app.presentation.extensions.showToastShort
import kz.frossu.bookingo.app.presentation.extensions.visible
import kz.frossu.bookingo.app.presentation.ui.adapters.CategorySpinnerAdapter

@AndroidEntryPoint
class BusinessSignUpFragment : Fragment() {

    val viewModel: BusinessSignUpViewModel by viewModels()
    private var _binding: FragmentBusinessSignUpBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    private lateinit var arrayAdapter: ArrayAdapter<CategoryDto>

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentBusinessSignUpBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val categoriesSpinner = binding.categorySpinner
        arrayAdapter = CategorySpinnerAdapter(requireContext(), listOf())
        categoriesSpinner.adapter = arrayAdapter

        setupObservers()
        setupSignInAndOutListeners()
        attachFormChangeListener()
        setupTimePicker()
    }

    private fun attachFormChangeListener() {
        with(binding) {
            val afterTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    // ignore
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    // ignore
                }

                override fun afterTextChanged(s: Editable?) {
                    viewModel.signUpDataChanged(
                        email.text.toString(),
                        password.text.toString(),
                        businessName.text.toString(),
                        businessDescription.text.toString(),
                    )
                }
            }
            email.addTextChangedListener(afterTextChangedListener)
            password.addTextChangedListener(afterTextChangedListener)
            businessName.addTextChangedListener(afterTextChangedListener)
            businessDescription.addTextChangedListener(afterTextChangedListener)
        }
    }

    private fun setupObservers() {
        viewModel.categories.observe(viewLifecycleOwner) { categories ->
            arrayAdapter.clear()
            arrayAdapter.addAll(categories)
        }

        viewModel.userSignUpFormState.observe(viewLifecycleOwner) { signUpFormState ->
            if (signUpFormState == null) {
                return@observe
            }
            binding.businessSignUp.isEnabled = signUpFormState.isDataValid
            signUpFormState.emailError?.let {
                binding.email.error = getString(it)
            }
            signUpFormState.passwordError?.let {
                binding.password.error = getString(it)
            }
            signUpFormState.businessNameError?.let {
                binding.businessName.error = getString(it)
            }
            signUpFormState.businessDescription?.let {
                binding.businessDescription.error = getString(it)
            }
        }

        viewModel.userSignUpResult.observe(viewLifecycleOwner) { signUpResult ->
            signUpResult ?: return@observe
            binding.loading.gone()
            signUpResult.error?.let {
                showSignUpFailed(it)
            }
            signUpResult.success?.let {
                updateUIWithUser(it)
                val action =
                    BusinessSignUpFragmentDirections.actionBusinessSignUpFragmentToBusinessSignInFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun setupSignInAndOutListeners() {
        binding.businessSignIn.setOnClickListener { v ->
            val action =
                BusinessSignUpFragmentDirections.actionBusinessSignUpFragmentToBusinessSignInFragment()
            v.findNavController().navigate(action)
        }

        binding.businessSignUp.setOnClickListener {
            binding.loading.visible()
            signUp()
        }

        binding.password.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signUp()
            }
            false
        }
    }

    private fun signUp() {
        val categoryDto = binding.categorySpinner.selectedItem as CategoryDto?

        if (categoryDto == null) {
            showToastShort("Please select a category")
            return
        }

        if (!binding.workdayStartTime.text.toString().contains(":") ||
            !binding.workdayEndTime.text.toString().contains(":")
        ) {
            showToastShort("Please specify workday start and end times")
            return
        }

        viewModel.signUp(
            binding.email.text.toString(),
            binding.password.text.toString(),
            binding.businessName.text.toString(),
            binding.businessDescription.text.toString(),
            categoryDto.id
        )
    }

    private fun setupTimePicker() {
        binding.workdayStartTime.setOnClickListener {
            it as TextView
            showTimePicker(10, 10, it, true)
        }

        binding.workdayEndTime.setOnClickListener {
            it as TextView
            showTimePicker(20, 20, it, false)
        }
    }

    private fun showTimePicker(
        startHour: Int,
        startMinute: Int,
        textView: TextView,
        isWorkdayStart: Boolean
    ) {

        val tv: TextView = textView
        val timePickerDialog = TimePickerDialog(
            requireContext(),
            { _, hourOfDay, minute ->
                tv.text = getString(R.string.hour_minute_formatted, hourOfDay, minute)
            }, startHour, startMinute, true
        )
//        timePickerDialog.updateTime(startHour, startMinute)
        timePickerDialog.show()
    }

    private fun showSignUpFailed(errorString: String) {
        showToastShort(errorString)
    }

    private fun updateUIWithUser(@StringRes stringRes: Int) {
        showToastShort(stringRes)
    }
}