package kz.frossu.bookingo.app.presentation.ui.fragments.map

import androidx.lifecycle.*
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.data.remote.apiservices.BookingoApiService
import kz.frossu.bookingo.app.data.remote.dtos.SubscriptionDto
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessInfo
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class BusinessDetailsViewModel @Inject constructor(
    private val bookingoApiService: BookingoApiService,
    savedStateHandle: SavedStateHandle
) : ViewModel() {
    val businessInfo: BusinessInfo =
        BusinessDetailsFragmentArgs.fromSavedStateHandle(savedStateHandle).businessInfo

    private val _subscriptionResult = MutableLiveData<Result<Unit>>()

    val subscriptionResult: LiveData<Result<Unit>> = _subscriptionResult

    private val _unsubscribeResult = MutableLiveData<Result<Unit>>()

    val unsubscribeResult: LiveData<Result<Unit>> = _unsubscribeResult

    fun subscribe() {
        viewModelScope.launch {
            try {
                Timber.d("SUBSCRIPTION: ${businessInfo.id}")
                bookingoApiService.subscribeToBusiness(SubscriptionDto(businessInfo.id))
                _subscriptionResult.value = Result.Success(data = Unit)
                businessInfo.isSubscribed = true
            } catch (e: Exception) {
                _subscriptionResult.value = Result.Error(exception = e)
                Timber.e(e)
            }
        }
    }

    fun unsubscribe() {
        viewModelScope.launch {
            try {
                Timber.d("UNSUBSRIBE ${businessInfo.id}")
                bookingoApiService.cancelSubscriptionToBusiness(SubscriptionDto(businessInfo.id))
                _unsubscribeResult.value = Result.Success(data = Unit)
                businessInfo.isSubscribed = false
            } catch (e: Exception) {
                _unsubscribeResult.value = Result.Error(exception = e)
                Timber.e(e)
            }
        }
    }

    fun isSubscribed(): Boolean {
        return businessInfo.isSubscribed
    }
}