package kz.frossu.bookingo.app.presentation.ui.fragments.map

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import dagger.hilt.android.AndroidEntryPoint
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.databinding.FragmentBusinessDetailsBinding
import kz.frossu.bookingo.app.presentation.extensions.invisible
import kz.frossu.bookingo.app.presentation.extensions.showToastLong
import kz.frossu.bookingo.app.presentation.extensions.visible
import kz.frossu.bookingo.app.presentation.ui.activity.MainActivity
import timber.log.Timber

@AndroidEntryPoint
class BusinessDetailsFragment : Fragment() {

    private val viewModel: BusinessDetailsViewModel by viewModels()

    private var _binding: FragmentBusinessDetailsBinding? = null

    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentBusinessDetailsBinding.inflate(inflater)
        setHasOptionsMenu(true)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (arguments != null) {
            binding.viewModel = viewModel
        }
        setupSubscriptionButtons()
    }

    private fun setupSubscriptionButtons() {

        Timber.d("KEK: ${viewModel.isSubscribed()}")
        if (viewModel.isSubscribed()) {
            binding.subscribe.invisible()
            binding.cancelSubscription.visible()
        } else {
            binding.subscribe.visible()
            binding.cancelSubscription.invisible()
        }

        binding.subscribe.setOnClickListener {
            if ((requireActivity() as MainActivity).isAuthorized()) {
                viewModel.subscribe()
            } else {
                showToastLong("Not logged in")
            }
        }

        binding.cancelSubscription.setOnClickListener {
            if ((requireActivity() as MainActivity).isAuthorized()) {
                viewModel.unsubscribe()
            } else {
                showToastLong("Not logged in")
            }
        }

        viewModel.subscriptionResult.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success -> {
                    binding.subscribe.invisible()
                    binding.cancelSubscription.visible()
                }
                is Result.Error -> {
                    val msg: String = result.exception.message ?: "unknown error"
                    showToastLong("error occurred during subscription: $msg")
                }
            }
        }
        viewModel.unsubscribeResult.observe(viewLifecycleOwner) { result ->
            when (result) {
                is Result.Success -> {
                    binding.subscribe.visible()
                    binding.cancelSubscription.invisible()
                }
                is Result.Error -> {
                    val msg: String = result.exception.message ?: "unknown error"
                    showToastLong("error occurred during subscription: $msg")
                }
            }
        }
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        inflater.inflate(R.menu.business_details_menu, menu)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}