package kz.frossu.bookingo.app.presentation.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kz.frossu.bookingo.app.data.repositories.LoginRepository
import kz.frossu.bookingo.app.presentation.models.LoggedInUser
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class LoggedInUserViewModel @Inject constructor(
    private val loginRepository: LoginRepository
) : ViewModel() {

    private val _currentUser = MutableLiveData<LoggedInUser?>()

    val currentUser: LiveData<LoggedInUser?>
        get() = _currentUser

    init {
        fetchCurrentUserIfAuthorized()
    }

    fun fetchCurrentUserIfAuthorized() {
        viewModelScope.launch {
            try {
                _currentUser.value = loginRepository.getUser()
            } catch (e: Exception) {
                _currentUser.value = null
            }
        }
    }

    fun signOut() {
        viewModelScope.launch {
            try {
                loginRepository.logout()
                _currentUser.value = null
            } catch (e: Exception) {
                Timber.d("Error occurred while logging out")
            }
        }
    }
}