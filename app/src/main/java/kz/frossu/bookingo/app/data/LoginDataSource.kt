package kz.frossu.bookingo.app.data

import kz.frossu.bookingo.app.data.remote.apiservices.AuthenticatorApiService
import kz.frossu.bookingo.app.data.remote.dtos.SignInRequest
import kz.frossu.bookingo.app.data.remote.dtos.tokens.Tokens
import timber.log.Timber
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class that handles authentication w/ userLogin credentials and retrieves user information.
 */
@Singleton
class LoginDataSource @Inject constructor(
    private val authService: AuthenticatorApiService,
) {

    suspend fun login(username: String, password: String): Result<Tokens> {
        return try {
            val tokens = authService.userSignIn(SignInRequest(username, password))
            Timber.d(tokens.token)
            Result.Success(tokens)
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
    }

    suspend fun logout() {}
}