package kz.frossu.bookingo.app.presentation.ui.fragments.profile.business

import androidx.fragment.app.Fragment
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class BusinessProfileFragment : Fragment() {
}