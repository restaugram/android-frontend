package kz.frossu.bookingo.app

import android.app.Application
import androidx.appcompat.app.AppCompatDelegate
import dagger.hilt.android.HiltAndroidApp
import kz.frossu.bookingo.app.data.local.preferences.PreferencesHelper
import timber.log.Timber
import javax.inject.Inject

@HiltAndroidApp
class App /*@Inject constructor(
    private val preferencesHelper: PreferencesHelper
)*/ : Application() {

    @Inject
    lateinit var preferencesHelper: PreferencesHelper

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }

        // TODO(android defense): 8.2 Application should be customizable by user
        val mode: Int = preferencesHelper.getNightMode()
        AppCompatDelegate.setDefaultNightMode(mode)
    }
}