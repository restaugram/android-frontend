package kz.frossu.bookingo.app.domain.models

import kz.frossu.bookingo.app.data.remote.dtos.Address
import java.time.OffsetTime

data class Business(
    val id: String,
    val businessName: String,
    val businessEmail: String,
    val description: String,
    val phone: String,
    val address: Address,
    val workdayStart: OffsetTime,
    val workdayEnd: OffsetTime,
)