package kz.frossu.bookingo.app.data.remote.apiservices

import kz.frossu.bookingo.app.data.remote.dtos.BusinessSignInRequest
import kz.frossu.bookingo.app.data.remote.dtos.SignInRequest
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessSignUpRequest
import kz.frossu.bookingo.app.data.remote.dtos.tokens.Tokens
import kz.frossu.bookingo.app.data.remote.dtos.user.UserSignUpRequest
import retrofit2.http.Body
import retrofit2.http.Field
import retrofit2.http.GET
import retrofit2.http.POST

interface AuthenticatorApiService {

    companion object {
        const val USER_IDENTITY_PATH = "/api/client/user/identity/"
        const val BUSINESS_IDENTITY_PATH = "/api/client/business/identity/"
    }

    // Client Endpoints

    @POST(USER_IDENTITY_PATH + "SignUp")
    suspend fun userSignUp(
        @Body body: UserSignUpRequest
    )

    @POST(USER_IDENTITY_PATH + "SignIn")
    suspend fun userSignIn(
        @Body body: SignInRequest
    ): Tokens

    @GET(USER_IDENTITY_PATH + "PasswordReset")
    suspend fun userPasswordReset(
        @Field("email") email: String
    ): Void

    @POST(USER_IDENTITY_PATH + "ConfirmEmail/confirm")
    suspend fun usertConfirmEmail(
        @Field("userId") userId: String
    ): Void

    /**
     * This one resets password, I guess
     */
    @POST(USER_IDENTITY_PATH + "PostPasswordReset")
    suspend fun userPostPasswordReset(
        @Field("password") password: String,
        @Field("operationId") operationId: String
    ): Void

    // Business endpoints

    @POST(BUSINESS_IDENTITY_PATH + "SignUp")
    suspend fun businessSignUp(
        @Body body: BusinessSignUpRequest
    )

    @POST(BUSINESS_IDENTITY_PATH + "SignIn")
    suspend fun businessSignIn(
        @Body signInRequest: BusinessSignInRequest
    ): Tokens

    @POST(BUSINESS_IDENTITY_PATH + "PasswordReset")
    suspend fun businessPasswordReset(

    ): Void

    @POST(BUSINESS_IDENTITY_PATH + "ConfirmBusiness/confirm")
    suspend fun businessConfirmEmail(
        @Field("verificationCode") verificationCode: String
    ): Void
}