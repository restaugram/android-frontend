package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.`in`

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.data.repositories.LoginRepository
import kz.frossu.bookingo.app.presentation.models.LoggedInUser
import javax.inject.Inject

@HiltViewModel
class BusinessLoginViewModel @Inject constructor(
    private val loginRepository: LoginRepository
) : ViewModel() {

    private val _loginForm = MutableLiveData<BusinessLoginFormState>()
    val businessLoginFormState: LiveData<BusinessLoginFormState> = _loginForm

    private val _loginResult = MutableLiveData<BusinessLoginResult>()
    val businessLoginResult: LiveData<BusinessLoginResult> = _loginResult

    fun login(username: String, password: String) {
        // can be launched in a separate asynchronous job
        viewModelScope.launch {
            when (val result = loginRepository.businessLogin(username, password)) {
                is Result.Success<LoggedInUser> ->
                    _loginResult.value =
                        BusinessLoginResult(success = LoggedInBusinessView(displayName = result.data.displayName))
                else ->
                    _loginResult.value = BusinessLoginResult(error = R.string.login_failed)
            }
        }
    }

    fun loginDataChanged(username: String, password: String) {
        if (!isUserNameValid(username)) {
            _loginForm.value = BusinessLoginFormState(usernameError = R.string.invalid_username)
        } else if (!isPasswordValid(password)) {
            _loginForm.value = BusinessLoginFormState(passwordError = R.string.invalid_password)
        } else {
            _loginForm.value = BusinessLoginFormState(isDataValid = true)
        }
    }

    // A placeholder username validation check
    private fun isUserNameValid(username: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(username).matches()
    }

    // A placeholder password validation check
    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}