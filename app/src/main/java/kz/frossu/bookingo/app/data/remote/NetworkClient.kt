package kz.frossu.bookingo.app.data.remote

import kz.frossu.bookingo.app.data.remote.apiservices.AuthenticatorApiService
import kz.frossu.bookingo.app.data.remote.apiservices.BookingoApiService
import retrofit2.create
import javax.inject.Inject

class NetworkClient @Inject constructor(
    retrofitClient: RetrofitClient,
    okHttp: OkHttp,
    /*authenticator: OkHttp.TokenA,*/
    authorizationInterceptor: OkHttp.AuthorizationInterceptor
) {
    private val provideRetrofit = retrofitClient.provideRetrofit(
        okHttp.provideOkHttpClient(authorizationInterceptor = authorizationInterceptor)
    )

    fun provideBookingoApiService(): BookingoApiService = provideRetrofit.create(
        BookingoApiService::class.java
    )

    class AuthenticatorClient @Inject constructor(
        retrofitClient: RetrofitClient,
        okHttp: OkHttp
    ) {
        private val provideRetrofit = retrofitClient.provideRetrofit(
            okHttp.provideOkHttpClient()
        )

        fun provideAuthenticatorApiService(): AuthenticatorApiService = provideRetrofit.create(
            AuthenticatorApiService::class.java
        )
    }
}