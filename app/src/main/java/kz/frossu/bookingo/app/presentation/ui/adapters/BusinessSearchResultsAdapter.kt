package kz.frossu.bookingo.app.presentation.ui.adapters

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessInfo
import kz.frossu.bookingo.app.databinding.ListItemBusinessBinding

// TODO(android defense): 7.2 Use RecyclerView to show list of information
class BusinessSearchResultsAdapter :
    ListAdapter<BusinessInfo, RecyclerView.ViewHolder>(BusinessEntityDiffCallback()) {

    lateinit var clickHandler: (BusinessInfo) -> Unit

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return BusinessViewHolder(
            clickHandler,
            ListItemBusinessBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }

    inner class BusinessViewHolder(
        private val onClickListener: (BusinessInfo) -> Unit,
        private val binding: ListItemBusinessBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        init {
            binding.root.setOnClickListener {
                onClickListener(getItem(adapterPosition))
            }
        }

        fun bind(businessInfo: BusinessInfo) {
            binding.twBusinessName.text = businessInfo.businessName
            binding.twBusinessDescription.text = businessInfo.description
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val business = getItem(position)
        (holder as BusinessViewHolder).bind(business)
    }
}

private class BusinessEntityDiffCallback : DiffUtil.ItemCallback<BusinessInfo>() {
    override fun areItemsTheSame(oldItem: BusinessInfo, newItem: BusinessInfo): Boolean {
        return oldItem.id == newItem.id
    }

    override fun areContentsTheSame(oldItem: BusinessInfo, newItem: BusinessInfo): Boolean {
        return oldItem == newItem
    }
}