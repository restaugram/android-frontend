package kz.frossu.bookingo.app.data.remote.apiservices

import kz.frossu.bookingo.app.data.remote.dtos.SubscriptionDto
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessDto
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessInfo
import kz.frossu.bookingo.app.data.remote.dtos.user.CategoryDto
import kz.frossu.bookingo.app.data.remote.dtos.user.UserDto
import retrofit2.http.*

interface BookingoApiService {

    companion object {
        private const val BASE_PATH = "/api/client/"
        const val USER_PATH = BASE_PATH + "user/"
        const val BUSINESS_PATH = BASE_PATH + "business/"
        const val CATEGORIES_PATH = BASE_PATH + "categories/" // why plural?
        const val BUSINESS_FETCH_PATH = BASE_PATH + "businessinfo/"

        const val BUSINESS_SEARCH_FOR_AUTHORIZED_USERS = USER_PATH + "fetchbusiness/"
    }

    @GET(USER_PATH)
    suspend fun fetchUser(): UserDto

    @PUT(USER_PATH)
    suspend fun editUser(
        @Body userDto: UserDto
    )

    @GET(CATEGORIES_PATH)
    suspend fun getCategories(): List<CategoryDto>

    @GET(BUSINESS_PATH)
    suspend fun fetchBusiness(): BusinessDto

    @GET(BUSINESS_FETCH_PATH + "search")
    suspend fun searchBusinesses(@Query("query") text: CharSequence): List<BusinessInfo>

    @GET(BUSINESS_SEARCH_FOR_AUTHORIZED_USERS)
    suspend fun searchBusinessesAuthorized(@Query("query") text: CharSequence): List<BusinessInfo>

    @POST(USER_PATH + "subscribe")
    suspend fun subscribeToBusiness(@Body subscriptionDto: SubscriptionDto)

    @POST(USER_PATH + "unsubscribe")
    suspend fun cancelSubscriptionToBusiness(@Body subscriptionDto: SubscriptionDto)

    @PUT(BUSINESS_PATH)
    suspend fun editBusiness(
        @Body businessDto: BusinessDto
    )

    // TODO(android defense): 2. All HTTP requests should be used (GET, POST, PUT, DELETE)
    @DELETE(USER_PATH)
    suspend fun deleteUser(userId: String)

    @DELETE(BUSINESS_PATH)
    suspend fun deleteBusiness(businessId: String)
}