package kz.frossu.bookingo.app.presentation.ui.fragments.auth.user.sign.up

import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import androidx.annotation.RequiresApi
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.navigation.findNavController
import androidx.navigation.fragment.findNavController
import dagger.hilt.android.AndroidEntryPoint
import kz.frossu.bookingo.app.databinding.FragmentUserSignUpBinding
import kz.frossu.bookingo.app.presentation.extensions.gone
import kz.frossu.bookingo.app.presentation.extensions.showToastShort
import kz.frossu.bookingo.app.presentation.extensions.visible

/**
 * A simple [Fragment] subclass.
 * Use the [ClientRegisterFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
@AndroidEntryPoint
class UserSignUpFragment : Fragment() {

    val viewModel: UserSignUpViewModel by viewModels()
    private var _binding: FragmentUserSignUpBinding? = null

    // This property is only valid between onCreateView and
    // onDestroyView.
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {

        _binding = FragmentUserSignUpBinding.inflate(layoutInflater)
        return binding.root
    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        setupObservers()
        setupSignInAndOutListeners()
        attachFormChangeListener()
    }

    private fun setupObservers() {
        viewModel.userSignUpFormState.observe(viewLifecycleOwner) { signUpFormState ->
            if (signUpFormState == null) {
                return@observe
            }
            binding.clientSignUp.isEnabled = signUpFormState.isDataValid
            signUpFormState.emailError?.let {
                binding.email.error = getString(it)
            }
            signUpFormState.passwordError?.let {
                binding.password.error = getString(it)
            }
            signUpFormState.phoneError?.let {
                binding.phoneNumber.error = getString(it)
            }
            signUpFormState.fullNameError?.let {
                binding.fullName.error = getString(it)
            }
        }

        viewModel.userSignUpResult.observe(viewLifecycleOwner) { signUpResult ->
            signUpResult ?: return@observe
            binding.loading.gone()
            signUpResult.error?.let {
                showSignUpFailed(it)
            }
            signUpResult.success?.let {
                updateUiWithUser(it)
                val action =
                    UserSignUpFragmentDirections.actionUserSignUpFragmentToUserSignInFragment()
                findNavController().navigate(action)
            }
        }
    }

    private fun setupSignInAndOutListeners() {
        binding.clientSignIn.setOnClickListener { v ->
            val action = UserSignUpFragmentDirections.actionUserSignUpFragmentToUserSignInFragment()
            v.findNavController().navigate(action)
        }

        binding.clientSignUp.setOnClickListener {
            binding.loading.visible()
            signUp()
        }

        binding.password.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                signUp()
            }
            false
        }
    }

    private fun signUp() {
        viewModel.signUp(
            binding.email.text.toString(),
            binding.password.text.toString(),
            binding.phoneNumber.text.toString(),
            binding.fullName.text.toString(),
        )
    }

    private fun attachFormChangeListener() {
        with(binding) {
            val afterTextChangedListener = object : TextWatcher {
                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    // ignore
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                    // ignore
                }

                override fun afterTextChanged(e: Editable) {
                    viewModel.signUpDataChanged(
                        email.text.toString(),
                        password.text.toString(),
                        phoneNumber.text.toString(),
                        fullName.text.toString(),
                    )
                }
            }
            email.addTextChangedListener(afterTextChangedListener)
            password.addTextChangedListener(afterTextChangedListener)
            phoneNumber.addTextChangedListener(afterTextChangedListener)
            fullName.addTextChangedListener(afterTextChangedListener)
        }
    }

    private fun showSignUpFailed(errorString: String) {
        showToastShort(errorString)
    }

    private fun updateUiWithUser(@StringRes stringRes: Int) {
        showToastShort(stringRes)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }
}
