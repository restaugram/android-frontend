package kz.frossu.bookingo.app.data.remote.dtos.business

import com.google.gson.annotations.SerializedName

data class BusinessDto(
    @SerializedName("id") val id: String,
    @SerializedName("businessName") var businessName: String,
    @SerializedName("businessEmail") var businessEmail: String,
    @SerializedName("description") var description: String? = null,
    @SerializedName("password") var password: String,
    @SerializedName("workDayStartTime") var workDayStartTime: String? = null,
    @SerializedName("workDayEndTime") var workDayEndTime: String? = null,
    @SerializedName("categoryId") var categoryId: String? = null
)