package kz.frossu.bookingo.app.data.remote.dtos.business

import com.google.gson.annotations.SerializedName

data class BusinessSignUpRequest(
    @SerializedName("businessName"     ) var businessName     : String,
    @SerializedName("description"      ) var description      : String,
    @SerializedName("businessEmail"    ) var businessEmail    : String,
    @SerializedName("password"         ) var password         : String,
    @SerializedName("workDayStartTime" ) var workDayStartTime : String,
    @SerializedName("workDayEndTime"   ) var workDayEndTime   : String,
    @SerializedName("categoryId"       ) var categoryId       : String
)