package kz.frossu.bookingo.app.data.remote.dtos.user

import com.google.gson.annotations.SerializedName
import java.time.LocalDateTime
import java.time.ZonedDateTime


data class UserSignUpRequest(
    @field:SerializedName("email") val email: String,
    @field:SerializedName("fullName") val fullName: String,
    @field:SerializedName("phone") val phone: String,
    @field:SerializedName("password") val password: String,
    @field:SerializedName("birthDate") val birthDate: String
)
