package kz.frossu.bookingo.app.data.remote.dtos

import com.google.gson.annotations.SerializedName

data class SignInRequest (
    @field:SerializedName("email") var email: String,
    @field:SerializedName("password") var password: String,
)