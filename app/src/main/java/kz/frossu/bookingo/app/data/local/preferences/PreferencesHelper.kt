package kz.frossu.bookingo.app.data.local.preferences

import android.content.Context
import android.content.SharedPreferences
import androidx.appcompat.app.AppCompatDelegate

class PreferencesHelper(context: Context) {

    private val preferences: SharedPreferences =
        context.getSharedPreferences("bookingo.preferences", Context.MODE_PRIVATE)

    companion object {
        const val USER_TOKEN = "user_token"
        const val IS_USER_CLIENT = "is_user_client"
        const val NIGHT_MODE = "night_mode"
    }

    fun getAccessToken(): String? {
        return preferences.getString(USER_TOKEN, null)
    }

    fun saveUser(token: String, isClient: Boolean) {
        saveAccessToken(token)
        setIsUserClient(isClient)
    }

    fun clearUser() {
        clearAccessToken()
        clearIsUserClient()
    }

    fun isClient(): Boolean {
        return preferences.getBoolean(IS_USER_CLIENT, false)
    }

    private fun saveAccessToken(token: String) {
        preferences.edit().putString(USER_TOKEN, token).apply()
    }

    private fun clearAccessToken() {
        preferences.edit().remove(USER_TOKEN).apply()
    }

    private fun setIsUserClient(isClient: Boolean) {
        preferences.edit().putBoolean(IS_USER_CLIENT, isClient).apply()
    }

    private fun clearIsUserClient() {
        preferences.edit().remove(IS_USER_CLIENT).apply()
    }

    // TODO(android defense): 8.3 Application should be customizable by user

    fun getNightMode(): Int {
        return preferences.getInt(NIGHT_MODE, AppCompatDelegate.MODE_NIGHT_NO)
    }

    fun setNightMode(mode: Int) {
        if (mode == AppCompatDelegate.MODE_NIGHT_YES ||
            mode == AppCompatDelegate.MODE_NIGHT_NO
        ) {
            preferences.edit().putInt(NIGHT_MODE, mode).apply()
        } else {
            throw IllegalArgumentException("Mode is not supported")
        }
    }
}