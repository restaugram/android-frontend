package kz.frossu.bookingo.app.data.remote.dtos

import com.google.gson.annotations.SerializedName

data class SubscriptionDto(
    @field:SerializedName("businessId") var businessId: CharSequence
)
