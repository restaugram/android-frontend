package kz.frossu.bookingo.app.presentation.ui.activity

import android.os.Handler
import android.os.Looper
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.data.remote.apiservices.BookingoApiService
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessInfo
import kz.frossu.bookingo.app.data.repositories.LoginRepository
import kz.frossu.bookingo.app.presentation.extensions.postDelayedWithToken
import kz.frossu.bookingo.app.presentation.models.LoggedInUser
import timber.log.Timber
import javax.inject.Inject

@HiltViewModel
class MapsViewModel @Inject constructor(
    private val loginRepository: LoginRepository,
    private val bookingoApiService: BookingoApiService,
) : ViewModel() {

    private val handler: Handler by lazy {
        Handler(Looper.getMainLooper())
    }

    private val _loading = MutableLiveData<Boolean>()

    val loading: LiveData<Boolean>
        get() = _loading

    private val _currentUser = MutableLiveData<LoggedInUser?>()

    val currentUser: LiveData<LoggedInUser?>
        get() = _currentUser

    fun isUserAuthorized(): Boolean {
        return currentUser.value != null
    }

    private val _searchData = MutableLiveData<List<BusinessInfo>>()

    val searchData: LiveData<List<BusinessInfo>>
        get() = _searchData

    init {
        fetchCurrentUserIfAuthorized()
        showSearchHistoryEntries()
    }

    fun fetchCurrentUserIfAuthorized() {
        viewModelScope.launch {
            try {
                _currentUser.value = loginRepository.getUser()
            } catch (e: Exception) {
                _currentUser.value = null
            }
        }
    }

    fun signOut() {
        viewModelScope.launch {
            try {
                loginRepository.logout()
                _currentUser.value = null
            } catch (e: Exception) {
                Timber.d("Error occurred while logging out")
            }
        }
    }

    private val animToken = Any()

    fun searchTextChanged(text: CharSequence?) {
//        handler.removeCallbacks(makeSearch)
        handler.removeCallbacksAndMessages(animToken)
        if (text != null) {
            if (text.isEmpty()) {
                // show history instead of results
                showSearchHistoryEntries()
                return
            } else {
                handler.postDelayedWithToken(animToken, 500) {
                    viewModelScope.launch {
                        val results: Result<List<BusinessInfo>> = search(text)
                        if (results is Result.Success) {
                            _searchData.value = results.data
                        } else {
                            Timber.e("Error while querying businesses")
                        }
                    }
                }
            }
        }
    }

    private suspend fun search(text: CharSequence): Result<List<BusinessInfo>> {
        return try {
            if (isUserAuthorized()) {

                // TODO: ask backenders to fix searching as authorized business
                //  'coz now, it only works for authorized users
                if (currentUser.value != null && !currentUser.value!!.isClient) {
                    Result.Success(bookingoApiService.searchBusinesses(text))
                }

                Result.Success(bookingoApiService.searchBusinessesAuthorized(text))
            } else {
                Result.Success(bookingoApiService.searchBusinesses(text))
            }
        } catch (e: Exception) {
            Timber.e(e)
            Result.Error(e)
        }
    }

    // TODO: Use local database with Room for search history
    private fun showSearchHistoryEntries() {
        _searchData.value = listOf(
            BusinessInfo(
                "abcde",
                "Bar Louie",
                "Eat. Drink. Be Happy. The tenets of the Original Gastrobar. Great drinks, chef-inspired food, and awesome service in a comfortable atmosphere where you are encouraged to relax and hang out. With Bar Louie, no two bars are going to be exactly the same. We listen to our neighborhoods\u200B and let them shape us, not the other way around. That’s what makes Bar Louie special. No corners cut. No excuses. It’s not always the easiest way to do things, but it’s the right way. Because if you’re not going to give people something special, why bother at all? So whether you’re looking for good food, a refreshing cocktail, or just conversation, Bar Louie has you covered.",
                "cool corp",
//                "+7-777-777-77-77",
//                Address("asbfsd", "Pushkina 27", 20.00, 23.45),
//                OffsetTime.of(10, 0, 0, 0, ZoneOffset.ofHours(6)),
//                OffsetTime.of(18, 0, 0, 0, ZoneOffset.ofHours(6))
            ),
            BusinessInfo(
                "abcdef",
                "MoonDollar",
                "Whether you're searching for something new to warm your mug, seeking the best brew method for your favourite blend or exploring our rarest offerings, you’ve come to the right place.",
                "cool corp",
//                "+7-777-777-77-77",
//                Address("asbfsd", "Pushkina 43", 20.00, 23.45),
//                OffsetTime.of(10, 0, 0, 0, ZoneOffset.ofHours(6)),
//                OffsetTime.of(18, 0, 0, 0, ZoneOffset.ofHours(6))
            ),
        )
    }
}