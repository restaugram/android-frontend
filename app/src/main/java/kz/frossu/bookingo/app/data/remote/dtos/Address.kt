package kz.frossu.bookingo.app.data.remote.dtos

data class Address(
    val id: String,
    val name: String,
    val latitude: Double,
    val longitude: Double
)