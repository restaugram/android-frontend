package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.up

import android.util.Patterns
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import kz.frossu.bookingo.app.R
import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.data.remote.apiservices.BookingoApiService
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessSignUpRequest
import kz.frossu.bookingo.app.data.remote.dtos.user.CategoryDto
import kz.frossu.bookingo.app.data.repositories.LoginRepository
import timber.log.Timber
import java.time.ZonedDateTime
import javax.inject.Inject

@HiltViewModel
class BusinessSignUpViewModel @Inject constructor(
    private val loginRepository: LoginRepository,
    private val bookingoApiService: BookingoApiService
) : ViewModel() {

    private val _categories = MutableLiveData<List<CategoryDto>>()

    val categories: LiveData<List<CategoryDto>>
        get() = _categories

    private val _signUpForm = MutableLiveData<BusinessSignUpFormState>()
    val userSignUpFormState: LiveData<BusinessSignUpFormState> = _signUpForm

    private val _signUpResult = MutableLiveData<BusinessSignUpResult>()
    val userSignUpResult: LiveData<BusinessSignUpResult> = _signUpResult

    init {
        fetchCategories()
    }

    fun signUpDataChanged(
        email: String,
        password: String,
        businessName: String,
        businessDescription: String
    ) {

        if (!isBusinessNameValid(businessName)) {
            _signUpForm.value =
                BusinessSignUpFormState(businessNameError = R.string.invalid_business_name)
        } else if (!isBusinessDescriptionValid(businessDescription)) {
            _signUpForm.value =
                BusinessSignUpFormState(businessDescription = R.string.invalid_business_description)
        } else if (!isEmailValid(email)) {
            _signUpForm.value = BusinessSignUpFormState(emailError = R.string.invalid_email)
        } else if (!isPasswordValid(password)) {
            _signUpForm.value = BusinessSignUpFormState(passwordError = R.string.invalid_password)
        } else {
            _signUpForm.value = BusinessSignUpFormState(isDataValid = true)
        }
    }

    fun signUp(
        email: String,
        password: String,
        businessName: String,
        businessDescription: String,
        categoryId: String,
        workDayStartTime: String = fakeStartDateTimeStr(),
        workDayEndTime: String = fakeEndDateTimeStr()
    ) {
        viewModelScope.launch {
            val businessSignUpRequest = BusinessSignUpRequest(
                businessName,
                businessDescription,
                email,
                password,
                workDayStartTime, workDayEndTime,
                categoryId
            )

            when (val result = loginRepository.businessSignUp(businessSignUpRequest)) {
                is Result.Success -> _signUpResult.value =
                    BusinessSignUpResult(success = R.string.business_sign_up_successful)
                is Result.Error -> _signUpResult.value =
                    BusinessSignUpResult(error = result.exception.message)
            }
        }
    }

    private fun fakeStartDateTimeStr(): String {
//        OffsetTime.of(10, 0, 0, 0, ZoneOffset.ofHours(6)),
//        LocalTime.now().toSecondOfDay() could have been better
        // backend doesn't accept this format lol
//        return ZonedDateTime.now().toString() // "2022-05-12T23:40:52.671+06:00[Asia/Almaty]"
        return "2022-05-12T17:42:57.019Z"
    }

    private fun fakeEndDateTimeStr(): String {
//        OffsetTime.of(10, 0, 0, 0, ZoneOffset.ofHours(6)),
//        LocalTime.now().toSecondOfDay() could have been better
        // backend doesn't accept this format lol
//        return ZonedDateTime.now().toString() // "2022-05-12T23:40:52.671+06:00[Asia/Almaty]"
        return "2022-05-12T17:42:57.019Z"
    }

    private fun fetchCategories() {
        viewModelScope.launch {
            try {
                _categories.value = bookingoApiService.getCategories()
                Timber.d("Fetched ${categories.value?.size} categories")
            } catch (e: Exception) {
                _categories.value = listOf()
                Timber.d("Failed to fetch categories")
            }
        }
    }

    private fun isBusinessNameValid(businessName: String): Boolean {
        return businessName.isNotBlank()
    }

    private fun isBusinessDescriptionValid(businessDescription: String): Boolean {
        return businessDescription.isNotBlank()
    }

    private fun isEmailValid(email: String): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    private fun isPasswordValid(password: String): Boolean {
        return password.length > 5
    }
}