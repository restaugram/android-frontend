package kz.frossu.bookingo.app.data.remote.dtos.user

import com.google.gson.annotations.SerializedName

data class UserDto(
    @SerializedName("id"        ) val id        : String,
    @SerializedName("email"     ) var email     : String,
    @SerializedName("fullName"  ) var fullName  : String,
    @SerializedName("phone"     ) var phone     : String? = null,
    @SerializedName("rating"    ) var rating    : Int?    = null,
    @SerializedName("birthDate" ) var birthDate : String? = null,
)