package kz.frossu.bookingo.app.presentation.ui.fragments.auth.user.sign.up

data class UserSignUpResult(
    val success: Int? = null,
    val error: String? = null
)