package kz.frossu.bookingo.app.presentation.ui.fragments.auth.business.sign.`in`

/**
 * Data validation state of the userLogin form.
 */
data class BusinessLoginFormState(
    val usernameError: Int? = null,
    val passwordError: Int? = null,
    val isDataValid: Boolean = false
)