package kz.frossu.bookingo.app.presentation.ui.fragments.auth

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.Fragment
import androidx.navigation.findNavController
import kz.frossu.bookingo.app.R

/**
 * A simple [Fragment] subclass.
 * Use the [AuthFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class AuthFragment : Fragment() {

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        val view = inflater.inflate(R.layout.fragment_auth, container, false)

        val clientSignInButton = view.findViewById<Button>(R.id.client_sign_in)
        val clientSignUpButton = view.findViewById<Button>(R.id.client_sign_up)
        val businessSignInButton = view.findViewById<Button>(R.id.business_sign_in)
        val businessSignUpButton = view.findViewById<Button>(R.id.business_sign_up)
        clientSignInButton.setOnClickListener { v ->
            val action = AuthFragmentDirections.actionAuthFragmentToUserSignInFragment()
            v.findNavController().navigate(action)
//            parentFragmentManager.commit {
//                replace<UserSignInFragment>(R.id.auth_fragment_container)
//                setReorderingAllowed(true)
//                addToBackStack(null)
//            }
        }
        clientSignUpButton.setOnClickListener { v ->
            val action = AuthFragmentDirections.actionAuthFragmentToUserSignUpFragment()
            v.findNavController().navigate(action)
//            parentFragmentManager.commit {
//                replace<UserSignUpFragment>(R.id.auth_fragment_container)
//                setReorderingAllowed(true)
//                addToBackStack(null)
//            }
        }
        businessSignInButton.setOnClickListener { v ->
            val action = AuthFragmentDirections.actionAuthFragmentToBusinessSignInFragment()
            v.findNavController().navigate(action)
//            parentFragmentManager.commit {
//                replace<BusinessSignInFragment>(R.id.auth_fragment_container)
//                setReorderingAllowed(true)
//                addToBackStack(null)
//            }
        }
        businessSignUpButton.setOnClickListener { v ->
            val action = AuthFragmentDirections.actionAuthFragmentToBusinessSignUpFragment()
            v.findNavController().navigate(action)
//            parentFragmentManager.commit {
//                replace<BusinessSignUpFragment>(R.id.auth_fragment_container)
//                setReorderingAllowed(true)
//                addToBackStack(null)
//            }
        }
        return view
    }
}