package kz.frossu.bookingo.app.data.remote.dtos.tokens

class Tokens (
    val token: String
)