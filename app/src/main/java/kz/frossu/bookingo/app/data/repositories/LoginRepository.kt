package kz.frossu.bookingo.app.data.repositories

import kz.frossu.bookingo.app.data.Result
import kz.frossu.bookingo.app.data.local.preferences.PreferencesHelper
import kz.frossu.bookingo.app.data.remote.apiservices.AuthenticatorApiService
import kz.frossu.bookingo.app.data.remote.apiservices.BookingoApiService
import kz.frossu.bookingo.app.data.remote.dtos.BusinessSignInRequest
import kz.frossu.bookingo.app.data.remote.dtos.SignInRequest
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessDto
import kz.frossu.bookingo.app.data.remote.dtos.business.BusinessSignUpRequest
import kz.frossu.bookingo.app.data.remote.dtos.tokens.Tokens
import kz.frossu.bookingo.app.data.remote.dtos.user.UserDto
import kz.frossu.bookingo.app.data.remote.dtos.user.UserSignUpRequest
import kz.frossu.bookingo.app.presentation.extensions.stringSuspending
import kz.frossu.bookingo.app.presentation.models.LoggedInUser
import retrofit2.HttpException
import java.io.IOException
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Class that requests authentication and user information from the remote data source and
 * maintains an in-memory cache of userLogin status and user credentials information.
 */

@Singleton
class LoginRepository @Inject constructor(
//    private val dataSource: LoginDataSource,
    private val authenticatorApiService: AuthenticatorApiService,
    private val bookingoApiService: BookingoApiService,
    private val preferencesHelper: PreferencesHelper
) {

    // in-memory cache of the loggedInUser object
    private var user: LoggedInUser? = null
//        private set

    val isLoggedIn: Boolean
        get() = user != null

    /**
     * This is the place where the code starts to smell :c
     * A LOT of if's alert!
     */
    suspend fun getUser(): LoggedInUser? {

        if (user == null && preferencesHelper.getAccessToken() != null) {
            val isClient = preferencesHelper.isClient()

            if (isClient) {
                val result: Result<UserDto> = fetchUser()
                if (result is Result.Success) {
                    user = LoggedInUser(result.data.id, result.data.fullName, isClient)
                }
            } else {
                val result: Result<BusinessDto> = fetchBusiness()
                if (result is Result.Success) {
                    user = LoggedInUser(result.data.id, result.data.businessName, isClient)
                }
            }
        }
        return user
    }

    init {
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
        user = null
    }

    fun logout() {
        user = null
        preferencesHelper.clearUser()
//        dataSource.logout()
    }

    suspend fun userLogin(username: String, password: String): Result<LoggedInUser> {
        // handle userLogin
        val tokensResult: Result<Tokens> = try {
            Result.Success(authenticatorApiService.userSignIn(SignInRequest(username, password)))
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
        if (tokensResult is Result.Success) {
            saveToken(tokensResult.data, true)
        } else {
            return Result.Error(IOException("Error while logging in"))
        }

        val result = fetchUser()
        return if (result is Result.Success) {
            val userDto = result.data
            val loggedInUser = LoggedInUser(userDto.id, userDto.fullName, true)
            setLoggedInUser(loggedInUser)
            Result.Success(loggedInUser)
        } else {
            Result.Error(IOException("Error fetching user"))
        }
    }

    private suspend fun fetchUser(): Result<UserDto> {
        return try {
            val userDto = bookingoApiService.fetchUser()
            Result.Success(userDto)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    suspend fun userSignUp(userSignUpRequest: UserSignUpRequest): Result<Unit> {
        return try {
            Result.Success(authenticatorApiService.userSignUp(userSignUpRequest))
        } catch (e: HttpException) {
            Result.Error(
                IOException(
                    "error while signing up: ${
                        e.response()?.errorBody()?.stringSuspending()
                    }"
                )
            )
        }
    }

    suspend fun businessLogin(username: String, password: String): Result<LoggedInUser> {
        val tokensResult: Result<Tokens> = try {
            Result.Success(
                authenticatorApiService.businessSignIn(
                    BusinessSignInRequest(
                        username,
                        password
                    )
                )
            )
        } catch (e: Throwable) {
            Result.Error(IOException("Error logging in", e))
        }
        if (tokensResult is Result.Success) {
            saveToken(tokensResult.data, false)
        } else {
            return Result.Error(IOException("Error while logging in"))
        }

        val result = fetchBusiness()
        return if (result is Result.Success) {
            val businessDto = result.data
            val loggedInUser = LoggedInUser(businessDto.id, businessDto.businessName, false)
            setLoggedInUser(loggedInUser)
            Result.Success(loggedInUser)
        } else {
            Result.Error(IOException("Error fetching user"))
        }
    }

    private suspend fun fetchBusiness(): Result<BusinessDto> {
        return try {
            val businessDto: BusinessDto = bookingoApiService.fetchBusiness()
            Result.Success(businessDto)
        } catch (e: Exception) {
            Result.Error(e)
        }
    }

    suspend fun businessSignUp(businessSignUpRequest: BusinessSignUpRequest): Result<Unit> {
        return try {
            Result.Success(authenticatorApiService.businessSignUp(businessSignUpRequest))
        } catch (e: HttpException) {
            Result.Error(
                IOException(
                    "error while signing up: ${
                        e.response()?.errorBody()?.stringSuspending()
                    }"
                )
            )
        }
    }

    private fun setLoggedInUser(loggedInUser: LoggedInUser) {
        this.user = loggedInUser
        // If user credentials will be cached in local storage, it is recommended it be encrypted
        // @see https://developer.android.com/training/articles/keystore
    }

    private fun saveToken(tokens: Tokens, isClient: Boolean) {
        preferencesHelper.saveUser(tokens.token, isClient)
    }
}